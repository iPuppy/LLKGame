//  [9/12/2013 Limo]
/************************************************************************/
/* 连连看游戏中点击的item	*/
/************************************************************************/
#pragma once
#include "util\GameHeader.h"
class ItemNode:public CCNode
{
public:
	//item的状态机
	typedef enum _itemState{
		Min=0,
		Normal,	//正常
		Select, //选中
		Destroy //销毁
	}ItemState; 
public:
	ItemNode(void);
	~ItemNode(void);

	virtual bool init();
	bool initItem(int index);	//初始化item
	static ItemNode* createIndexAndSize(int index,const cocos2d::CCSize contentSize);//根据当前传递的 整型值来取出当前的所对应的的图片
public:
	CC_SYNTHESIZE(int,_index,Index);//选中的编号
	CC_SYNTHESIZE(bool,_isAlive,IsAlive);//是否存活，true为存活 false为消失
	CC_SYNTHESIZE(int,_x,X);		//坐标x的位置(相对map)
	CC_SYNTHESIZE(int,_y,Y);		//坐标y的位置(相对map)
	CC_SYNTHESIZE_RETAIN(CCSprite*,_nodeSprite,NodeSprite);	//item显示的

	ItemNode::ItemState getState() const { return _state; }
	void setState(ItemNode::ItemState val); //设置状态机
	void DestroyFunction();		//销毁自身的方法

private:
	ItemState _state;
	

};

