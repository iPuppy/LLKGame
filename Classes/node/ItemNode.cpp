#include "ItemNode.h"


ItemNode::ItemNode(void)
{
	_isAlive=true;
	_x=0;
	_y=0;
	_nodeSprite=NULL;
	_state=Normal;
}


ItemNode::~ItemNode(void)
{
	_nodeSprite->release();
}

bool ItemNode::init()
{
	bool bRet=false;
	do 
	{
		CC_BREAK_IF(!CCNode::init());


		bRet=true;
	} while (0);

	return bRet;
}

ItemNode* ItemNode::createIndexAndSize( int index,const cocos2d::CCSize contentSize )
{
	ItemNode * pRet = new ItemNode();
	if (pRet)
	{
		pRet->setContentSize(contentSize);
	}

	if (pRet && pRet->init()&&pRet->initItem(index))
	{

		pRet->autorelease();
	}
	else
	{
		CC_SAFE_DELETE(pRet);
	}
	return pRet;
}

void ItemNode::setState( ItemState val )
{
	switch (val)
	{
	case Normal:{
		//正常状态
		//CCDirector::sharedDirector()->getActionManager()->removeAllActionsFromTarget(_nodeSprite);
		_nodeSprite->setScale(1);	
				}
				break;
	case Select:{
		//选中状态
		//CCDirector::sharedDirector()->getActionManager()->removeAllActionsFromTarget(_nodeSprite);
		//_nodeSprite->runAction(CCRepeatForever::create(CCBlink::create(1,4)));
		_nodeSprite->setScale(1.1f);	
		
				}
				break;
	case Destroy:{
		//销毁状态
		setIsAlive(false);
		_nodeSprite->setVisible(false);
		//CCDirector::sharedDirector()->getActionManager()->removeAllActionsFromTarget(_nodeSprite);
		//CCCallFunc* callBack=CCCallFunc::create(this,callfunc_selector(ItemNode::DestroyFunction));
		//_nodeSprite->runAction(CCSequence::create(CCFadeOut::create(.5f),callBack));
				}
				break;
	default:{
		//错误状态
			}
			break;
	}

}

bool ItemNode::initItem( int index )
{
	setIndex(index);
	char itemName[15]={0};
	sprintf(itemName,"item_%d.png",index);
	INIT_UP
		if(index==-1){

			setIsAlive(false);
			CCSprite* item=CCSprite::create();
			setNodeSprite(item);
			_nodeSprite->setAnchorPoint(ccp(0,0));
			_nodeSprite->setPosition(CCPointZero);
			this->addChild(_nodeSprite);		//加入
		}else{
			//初始化显示图片的地方
			CCSprite* item=CCSprite::create(itemName);
			setNodeSprite(item);
			_nodeSprite->setAnchorPoint(ccp(0,0));
			_nodeSprite->setPosition(CCPointZero);
			this->addChild(_nodeSprite);		//加入

		}


	INIT_DOWN
}

void ItemNode::DestroyFunction()
{
	_nodeSprite->setVisible(false);
}
