//  [9/13/2013 Limo]
/************************************************************************/
/* 控制的层，用于接收并且相应用户的操作*/
/************************************************************************/
#pragma once
#include "basic\BasicLayer.h"
#include "util\GameHeader.h"
class ControlLayer:public BasicLayer
{
public:
	ControlLayer(void);
	~ControlLayer(void);


	virtual bool init();
	CREATE_FUNC(ControlLayer);

	virtual void onEnter();
	virtual void onExit();
	virtual void registerWithTouchDispatcher( void );

	virtual bool ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent );
	virtual void ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent );
	virtual void ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent );
	virtual void ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent );


};

