//  [9/12/2013 Limo]
/************************************************************************/
/* 游戏层		*/
/************************************************************************/
#pragma once
#include "util\GameHeader.h"
#include "basic\BasicLayer.h"
#include "node\ItemNode.h"
class GameLayer:public BasicLayer
{
public:
	GameLayer(void);
	~GameLayer(void);

	virtual bool init();
	CREATE_FUNC(GameLayer);
	bool initMap();			//初始化地图
	bool initItems();		//初始化元素

	CC_SYNTHESIZE_RETAIN(CCArray*,_itemArray,ItemArray);	//游戏中的item数组,未混淆
	CC_SYNTHESIZE_RETAIN(CCArray*,_mapArray,MapArray);		//游戏中的map数组,混淆后包含空白部分
	CC_SYNTHESIZE_RETAIN(CCArray*,_mySelectArray,MySelectArray);//选择的数组
	CC_SYNTHESIZE_RETAIN(ItemNode*,_mySelectItem,MySelectItem);	//选择的数组
	CC_SYNTHESIZE_RETAIN(CCProgressTimer*,_progressTimer,ProgressTimer);	//选择的数组

	void CheckSelectItem(CCPoint location);			//判断是否点击到了 item

	ItemNode* getNodeByXAndY(CCArray* mapArray,int x,int y);	//通过X和Y获得item
	CCArray* pathArray(CCArray* mapArray,CCArray* selectArray); //获得的之间的可连接路径，不连接返回NULL
	CCArray* CheckNoCorner(CCArray* mapArray,CCArray* selectArray); //检测没有拐角
	CCArray* CheckOneCorner(CCArray* mapArray,CCArray* selectArray); //检测一个拐角
	CCArray* CheckTwoCorner(CCArray* mapArray,CCArray* selectArray); //检测两个拐角


private:
	int row; // 多少行
	int cloum;// 多少列

	int offerX;	//x轴的偏移值
	int offerY;	//y轴的偏移值
	int selectNum;	//选择的编号
};

