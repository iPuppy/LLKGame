//  [9/13/2013 Limo]
/************************************************************************/
/* 游戏的分数显示界面*/
/************************************************************************/
#pragma once
#include "util\GameHeader.h"
#include "basic\BasicLayer.h"
class GameScoreLayer:public BasicLayer
{
public:
	GameScoreLayer(void);
	~GameScoreLayer(void);

	virtual bool init();
	CREATE_FUNC(GameScoreLayer);

};

