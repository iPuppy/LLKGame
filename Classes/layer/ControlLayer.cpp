#include "ControlLayer.h"
#include "scene\GameScene.h"


ControlLayer::ControlLayer(void)
{
}


ControlLayer::~ControlLayer(void)
{
}

bool ControlLayer::init()
{
	INIT_UP
		CC_BREAK_IF(!BasicLayer::init());
		this->setTouchEnabled(true);		//���ô���

	INIT_DOWN
}

void ControlLayer::onEnter()
{
	BasicLayer::onEnter();

}

void ControlLayer::onExit()
{
	BasicLayer::onExit();
	CCDirector::sharedDirector()->getTouchDispatcher()->removeDelegate(this);
}

void ControlLayer::registerWithTouchDispatcher( void )
{
	CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,-1,false);
}

bool ControlLayer::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return true;
}

void ControlLayer::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{

}

void ControlLayer::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{
	CCPoint point=pTouch->getLocation();
	char location[30]={0};
	sprintf(location,"[%f,%f]",point.x,point.y);

	((GameScene*)this->getParent())->getMyGameLayer()->CheckSelectItem(point);

}

void ControlLayer::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{

}
