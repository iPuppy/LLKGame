#include "GameLayer.h"
#include "node\ItemNode.h"


GameLayer::GameLayer(void)
{
	row=8;
	cloum=12;
	offerX=32;
	offerY=0;

	selectNum=-1;
	_itemArray=NULL;
	_mapArray=NULL;
	//_mySelectItem=NULL;
	_mySelectArray=NULL;
	_progressTimer=NULL;
}


GameLayer::~GameLayer(void)
{
	_itemArray->release();
	_mapArray->release();
	//_mySelectItem->release();
	_mySelectArray->release();
	_progressTimer->release();
}

bool GameLayer::init()
{
	bool bRet=false;
	do 
	{
		CC_BREAK_IF(!BasicLayer::initWithColor(ccc4(255,255,255,120)));

		//初始化item数组
		setItemArray(CCArray::create());
		//初始化Map数组
		setMapArray(CCArray::create());

		setMySelectArray(CCArray::create());
		initItems();
		initMap();

		//加入进度条
		CCSprite* sp=CCSprite::create("pro1.png");
		CCProgressTimer* timer=CCProgressTimer::create(sp);
		timer->setType(kCCProgressTimerTypeBar);
		timer->setMidpoint(ccp(0,0));
		timer->setBarChangeRate(ccp(1, 0));
		timer->setPercentage(50);
		setProgressTimer(timer);
		_progressTimer->setPosition(getWinSize().width/2,getWinSize().height-40);
		this->addChild(_progressTimer,4);


		bRet=true;
	} while (0);
	return bRet;

}

bool GameLayer::initMap()
{
	for (int i=0;i<row+2;++i)
	{
		for (int j=0;j<cloum+2;++j)
		{
			if ((i==0)||(j==0)||(j==cloum+1)||(i==row+1)){
				ItemNode*  temp=  ItemNode::createIndexAndSize(-1,CCSize(64,64));
				temp->setX(j);
				temp->setY(i);
				temp->setAnchorPoint(ccp(0,0));
				temp->setPosition(ccp(64*(j)+offerX,64*(i)+offerY));
				this->addChild(temp);
				_mapArray->addObject(temp);

			}else {
				ItemNode* phit=(ItemNode*)_itemArray->randomObject();
				_itemArray->removeObject(phit,false);
				phit->setAnchorPoint(ccp(0,0));
				phit->setPosition(ccp(64*(j)+offerX,64*(i)+offerY));
				this->addChild(phit);
				_mapArray->addObject(phit);
				phit->setX(j);
				phit->setY(i);
				_mapArray->addObject(phit);
			}


		}
	}

	return true;
}

bool GameLayer::initItems()
{
	//加入数组
	for(int i=0,n=row*cloum/2;i<n;i++){
		int index=CCRANDOM_0_1()*11;
		ItemNode* itemNode1=ItemNode::createIndexAndSize(index,CCSizeMake(64,64));
		ItemNode* itemNode2=ItemNode::createIndexAndSize(index,CCSizeMake(64,64));
		_itemArray->addObject(itemNode1);
		_itemArray->addObject(itemNode2);
	}
	return true;

}

void GameLayer::CheckSelectItem( CCPoint location )
{
	for(int i=0;i<_mapArray->count();i++){
		ItemNode* temp=(ItemNode*)_mapArray->objectAtIndex(i);
		if(temp->boundingBox().containsPoint(location)){
			if(!temp->getIsAlive()){
				continue;
			}
			if(_mySelectArray->count()==0){
				_mySelectArray->addObject(temp);
				temp->setState(ItemNode::Select);
			}else {
				ItemNode* temp2=(ItemNode*) _mySelectArray->objectAtIndex(0);

				if (temp->getX()==temp2->getX()&&temp->getY()==temp2->getY()){
					break;
				}

				if(temp->getIndex()==temp2->getIndex()){
					_mySelectArray->addObject(temp);
					//_mySelectArray->removeObjectAtIndex(0,false);
					//temp->setState(ItemNode::Destroy);
					//temp2->setState(ItemNode::Destroy);
				}else {
					_mySelectArray->removeObjectAtIndex(0,false);
					_mySelectArray->addObject(temp);
					temp2->setState(ItemNode::Normal);
					temp->setState(ItemNode::Select);
				}

			} 
			break;
		}
	}
	//玩家店家两个同样id的图块
	if (_mySelectArray->count()==2)
	{
		ItemNode* node1=(ItemNode*)_mySelectArray->objectAtIndex(0);
		ItemNode* node2=(ItemNode*)_mySelectArray->objectAtIndex(1);
		//判断是否能消除
		CCArray* tempArray=pathArray(_mapArray,_mySelectArray);
		if(tempArray){
			node1->setState(ItemNode::Destroy);
			node2->setState(ItemNode::Destroy);
			_mySelectArray->removeObjectAtIndex(0,false);
			_mySelectArray->removeObjectAtIndex(0,false);

		}else{
			_mySelectArray->removeObjectAtIndex(0,false);
			node1->setState(ItemNode::Normal);
			node2->setState(ItemNode::Select);
		}

	}

}

ItemNode* GameLayer::getNodeByXAndY( CCArray* mapArray,int x,int y )
{
	for (int i=0;i<_mapArray->count();i++){
		ItemNode* item=(ItemNode*)_mapArray->objectAtIndex(i);
		if (item->getX()==x&&item->getY()==y){
			return item;
		}else {
			continue;
		}
	}
}

CCArray* GameLayer::pathArray( CCArray* mapArray,CCArray* selectArray )
{
	CCArray* path=NULL;
	path=CheckNoCorner(mapArray,selectArray);
	if(path){
		return path;
	}
	path=CheckOneCorner(mapArray,selectArray);
	if(path){
		return path;
	}
	path=CheckTwoCorner(mapArray,selectArray);
	if(path){
		return path;
	}

	return NULL;
}
/************************************************************************/
/* 检测没有拐点的算法*/
/************************************************************************/
CCArray* GameLayer::CheckNoCorner( CCArray* mapArray,CCArray* selectArray )
{
	ItemNode* item0=(ItemNode*)selectArray->objectAtIndex(0);
	ItemNode* item1=(ItemNode*)selectArray->objectAtIndex(1);
	CCArray*  returnArray=CCArray::create();

	if(item0->getX()==item1->getX()){
		//纵向位置相同
		bool flag=item0->getY()>item1->getY();
		//获得距离的范围
		int range=flag?item0->getY()-item1->getY():item1->getY()-item0->getY();
		if (range==1)
		{
			return returnArray;
		}else{
			if(flag){

				for (int i=item1->getY()+1,n=item0->getY();i<n;i++)
				{
					ItemNode* item=getNodeByXAndY(_mapArray,item0->getX(),i);
					if (item->getIsAlive())
					{
						return NULL;
					}else{
						returnArray->addObject(item);
					}

				}
			}else{
				for (int i=item0->getY()+1,n=item1->getY();i<n;i++)
				{
					ItemNode* item=getNodeByXAndY(_mapArray,item0->getX(),i);
					if (item->getIsAlive())
					{
						return NULL;
					}else{
						returnArray->addObject(item);
					}

				}

			}

			return returnArray;

		}


	}else if (item0->getY()==item1->getY())
	{
		//横向位置相同
		bool flag=item0->getX()>item1->getX();
		//获得距离的范围
		int range=flag?item0->getX()-item1->getX():item1->getX()-item0->getX();
		
		if (range==1)
		{
			return returnArray;
		}else{

			if(flag){
				
				for (int i=item1->getX()+1,n=item0->getX();i<n;i++)
				{
					ItemNode* item=getNodeByXAndY(_mapArray,i,item0->getY());
					if (item->getIsAlive())
					{
						return NULL;
					}else{
						returnArray->addObject(item);
					}

				}
			}else{
				for (int i=item0->getX()+1,n=item1->getX();i<n;i++)
				{
					ItemNode* item=getNodeByXAndY(_mapArray,i,item0->getY());
					if (item->getIsAlive())
					{
						return NULL;
					}else{
						returnArray->addObject(item);
					}

				}

			}

			return returnArray;
		}


	}

	return NULL;

}
/************************************************************************/
/* 检测一个拐点的算法*/
/************************************************************************/
CCArray* GameLayer::CheckOneCorner( CCArray* mapArray,CCArray* selectArray )
{
	ItemNode* item0=(ItemNode*)selectArray->objectAtIndex(0);
	ItemNode* item1=(ItemNode*)selectArray->objectAtIndex(1);
	CCArray* returnArray=CCArray::create();// 记录两个点之间 所经过的点

	//判断位置
	bool flagX=item0->getX()>item1->getX();
	bool flagY=item0->getY()>item1->getY();

	//x的range  y的range
	int rangeX=flagX?item0->getX()-item1->getX():item1->getX()-item0->getX();
	int rangeY=flagY?item0->getY()-item1->getY():item1->getY()-item0->getY();

	//折点位置
	int cornerX=0;
	int cornerY=0;

	//首先判断上面的拐点
	if(flagY){
		//第一个点在上面
		cornerX=item1->getX();
		cornerY=item0->getY();
	}else{
		//第一个点在下面
		cornerX=item0->getX();
		cornerY=item1->getY();
	}

	ItemNode* item=getNodeByXAndY(_mapArray,cornerX,cornerY);
	if(!item->getIsAlive()){
		//检测的数组
		CCArray* hitArray0=CCArray::create();
		hitArray0->addObject(item);
		hitArray0->addObject(item0);

		CCArray* hitArray1=CCArray::create();
		hitArray1->addObject(item);
		hitArray1->addObject(item1);
		
		returnArray=CheckNoCorner(_mapArray,hitArray0);
		if(returnArray!=NULL){
			CCArray* tempArray=CheckNoCorner(_mapArray,hitArray1);
			if(tempArray!=NULL){
				returnArray->addObjectsFromArray(tempArray);
				returnArray->addObject(item);
				return returnArray;
			}

		}

	}


	//然后判断下面的拐点
	if(flagY){
		//第一个点在上面
		cornerX=item0->getX();
		cornerY=item1->getY();
	}else{
		//第一个点在下面
		cornerX=item1->getX();
		cornerY=item0->getY();
	}

	ItemNode* newItem=getNodeByXAndY(_mapArray,cornerX,cornerY);
	if(!newItem->getIsAlive()){
		//检测的数组
		CCArray* hitArray0=CCArray::create();
		hitArray0->addObject(newItem);
		hitArray0->addObject(item0);

		CCArray* hitArray1=CCArray::create();
		hitArray1->addObject(newItem);
		hitArray1->addObject(item1);
		returnArray=CheckNoCorner(_mapArray,hitArray0);
		if(returnArray!=NULL){
			CCArray* tempArray=CheckNoCorner(_mapArray,hitArray1);
			if(tempArray!=NULL){
				returnArray->addObjectsFromArray(tempArray);
				returnArray->addObject(item);
				return returnArray;
			}

		}

	}


	return NULL;

}
/************************************************************************/
/* 检测两个个拐点的算法*/
/************************************************************************/
CCArray* GameLayer::CheckTwoCorner( CCArray* mapArray,CCArray* selectArray )
{
	ItemNode* item0=(ItemNode*)selectArray->objectAtIndex(0);
	ItemNode* item1=(ItemNode*)selectArray->objectAtIndex(1);
	CCArray* returnArray=CCArray::create();// 记录两个点之间 所经过的点

	//选中一个点，做4方向延展
	//选中的点为item0
	//上延伸
	for (int i=item0->getY()+1;i<row+2;++i)
	{
		CCArray* hitArray0=CCArray::create();	//没有拐点
		CCArray* hitArray1=CCArray::create();	//一个拐点
		
		//向上延伸的拐点探测
		ItemNode* item=getNodeByXAndY(_mapArray,item0->getX(),i);

		if(item->getIsAlive()){
			break;
		}

		hitArray0->addObject(item0);
		hitArray0->addObject(item);

		hitArray1->addObject(item);
		hitArray1->addObject(item1);

		returnArray=CheckOneCorner(_mapArray,hitArray1);
		if(!returnArray){
			continue;
		}

		//检测完成加入到returnArray  
		returnArray->addObjectsFromArray(CheckNoCorner(_mapArray,hitArray0));
		returnArray->addObject(item);
		return returnArray;
	}

	//下延伸
	for (int i=item0->getY()-1;i>=0;--i)
	{
		CCArray* hitArray0=CCArray::create();	//没有拐点
		CCArray* hitArray1=CCArray::create();	//一个拐点

		//向上延伸的拐点探测
		ItemNode* item=getNodeByXAndY(_mapArray,item0->getX(),i);

		if(item->getIsAlive()){
			break;
		}

		hitArray0->addObject(item0);
		hitArray0->addObject(item);

		hitArray1->addObject(item);
		hitArray1->addObject(item1);

		returnArray=CheckOneCorner(_mapArray,hitArray1);
		if(!returnArray){
			continue;
		}

		//检测完成加入到returnArray  
		returnArray->addObjectsFromArray(CheckNoCorner(_mapArray,hitArray0));
		returnArray->addObject(item);
		return returnArray;
	}

	//左延伸
	for (int i=item0->getX()-1;i>=0;--i)
	{
		CCArray* hitArray0=CCArray::create();	//没有拐点
		CCArray* hitArray1=CCArray::create();	//一个拐点

		//向上延伸的拐点探测
		ItemNode* item=getNodeByXAndY(_mapArray,i,item0->getY());

		if(item->getIsAlive()){
			break;
		}

		hitArray0->addObject(item0);
		hitArray0->addObject(item);

		hitArray1->addObject(item);
		hitArray1->addObject(item1);

		returnArray=CheckOneCorner(_mapArray,hitArray1);
		if(!returnArray){
			continue;
		}

		//检测完成加入到returnArray  
		returnArray->addObjectsFromArray(CheckNoCorner(_mapArray,hitArray0));
		returnArray->addObject(item);
		return returnArray;
	}

	//下延伸
	for (int i=item0->getY()-1;i>=0;--i)
	{
		CCArray* hitArray0=CCArray::create();	//没有拐点
		CCArray* hitArray1=CCArray::create();	//一个拐点

		//向上延伸的拐点探测
		ItemNode* item=getNodeByXAndY(_mapArray,item0->getX(),i);

		if(item->getIsAlive()){
			break;
		}

		hitArray0->addObject(item0);
		hitArray0->addObject(item);

		hitArray1->addObject(item);
		hitArray1->addObject(item1);

		returnArray=CheckOneCorner(_mapArray,hitArray1);
		if(!returnArray){
			continue;
		}

		//检测完成加入到returnArray  
		returnArray->addObjectsFromArray(CheckNoCorner(_mapArray,hitArray0));
		returnArray->addObject(item);
		return returnArray;
	}

	//右延伸
	for (int i=item0->getX()+1;i<cloum+2;++i)
	{
		CCArray* hitArray0=CCArray::create();	//没有拐点
		CCArray* hitArray1=CCArray::create();	//一个拐点

		//向上延伸的拐点探测
		ItemNode* item=getNodeByXAndY(_mapArray,i,item0->getY());

		if(item->getIsAlive()){
			break;
		}

		hitArray0->addObject(item0);
		hitArray0->addObject(item);

		hitArray1->addObject(item);
		hitArray1->addObject(item1);

		returnArray=CheckOneCorner(_mapArray,hitArray1);
		if(!returnArray){
			continue;
		}

		//检测完成加入到returnArray  
		returnArray->addObjectsFromArray(CheckNoCorner(_mapArray,hitArray0));
		returnArray->addObject(item);
		return returnArray;
	}

	return NULL;

}
