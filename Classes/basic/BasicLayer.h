/************************************************************************/
/* cocos2dx中层的基类，定义了一些工具方法，便于开发实现							*/
/************************************************************************/
#pragma once
#include "cocos2d.h"
USING_NS_CC;

class BasicLayer:public CCLayerColor
{
public:
	BasicLayer(void);
	~BasicLayer(void);
	virtual void onEnter();		
	virtual void onExit();
	virtual bool init();		//直接初始化的方法
	virtual bool initWithColor( const cocos2d::ccColor4B& color, 
		GLfloat width, GLfloat height );	//通过ccc4颜色和大小初始化的方法
	virtual bool initWithColor( const cocos2d::ccColor4B& color ); //通过ccc4颜色初始化的方法

	cocos2d::CCSize getWinSize();			//当前的屏幕尺寸
	cocos2d::CCPoint getMiddlePoint();		//当前屏幕的中点

};

