//  [9/18/2013 Limo]
/************************************************************************/
/* 基础的对话框，两个按钮的对话框												*/
/************************************************************************/
#pragma once
#include "BasicLayer.h"			//基础之基础层
class BasicDialog:public BasicLayer
{
public:
	BasicDialog(void);
	~BasicDialog(void);
	CREATE_FUNC(BasicDialog);
	virtual bool init();

	bool initDialog();		//初始化对话框

	virtual void onEnter();

	virtual void onExit();

	virtual bool ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent );

	virtual void ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent );

	virtual void ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent );

	virtual void ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent );

	virtual void registerWithTouchDispatcher( void );

	bool BuildeDialog(CCSprite* bg,CCMenuItem* submit,CCMenuItem* cancel);		//创建对话框的方法



private:
	// 模态对话框菜单  
	CCMenu *m_pMenu;  
	// 记录菜单点击  
	bool m_bTouchedMenu;  
	CCSprite* pBg;				//对话框背景
	CCMenuItem* submitButton;	//确定按钮
	CCMenuItem* cancelButton;	//取消按钮
};

