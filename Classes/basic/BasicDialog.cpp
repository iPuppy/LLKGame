#include "BasicDialog.h"
#include "util\GameHeader.h"


BasicDialog::BasicDialog(void)
{
}


BasicDialog::~BasicDialog(void)
{
}

bool BasicDialog::init()
{
	INIT_UP
		CC_BREAK_IF(!BasicLayer::initWithColor(ccc4(0,0,0,80)));
		CC_BREAK_IF(!initDialog());

	INIT_DOWN
}

void BasicDialog::onEnter()
{
	BasicLayer::onEnter();
}

void BasicDialog::onExit()
{
	BasicLayer::onExit();
	CCDirector::sharedDirector()->getTouchDispatcher()->removeDelegate(this);
}

bool BasicDialog::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	//是否点击到菜单
	m_bTouchedMenu = m_pMenu->ccTouchBegan(pTouch, pEvent);  
	return true;  
}

void BasicDialog::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{
	if (m_bTouchedMenu) {  
		m_pMenu->ccTouchMoved(pTouch, pEvent);  
	}  

}

void BasicDialog::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{


	if (m_bTouchedMenu) {  
		m_pMenu->ccTouchEnded(pTouch, pEvent);  
		m_bTouchedMenu = false;  
	}  
}

void BasicDialog::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{
	if (m_bTouchedMenu) {  
		m_pMenu->ccTouchCancelled(pTouch, pEvent);  
		m_bTouchedMenu = false;  
	}  
}

void BasicDialog::registerWithTouchDispatcher( void )
{
	//注册触摸事件
	CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this,kCCMenuHandlerPriority-1,true);
}

bool BasicDialog::initDialog()
{
	INIT_UP
	
		
	INIT_DOWN

}

bool BasicDialog::BuildeDialog( CCSprite* bg,CCMenuItem* submit,CCMenuItem* cancel )
{
INIT_UP
	//加入背景
	CC_BREAK_IF(!bg);
	bg->setPosition(getMiddlePoint());
	this->addChild(bg,0);
	//装载按钮
	CC_BREAK_IF(!submit);
	CC_BREAK_IF(!cancel);	
	m_pMenu=CCMenu::create(submit,cancel,NULL);
	CC_BREAK_IF(!m_pMenu);	
	bg->addChild(m_pMenu,1);
	m_pMenu->setPosition(bg->getContentSize().width/2,submit->getContentSize().height/4*3);
	m_pMenu->alignItemsHorizontallyWithPadding((bg->getContentSize().width-submit->getContentSize().width*2)/4);

INIT_DOWN
}
