#include "BasicLayer.h"

BasicLayer::BasicLayer(void)
{
}


BasicLayer::~BasicLayer(void)
{
}

cocos2d::CCSize BasicLayer::getWinSize()
{
	return CCDirector::sharedDirector()->getWinSize();
}

bool BasicLayer::init()
{
	bool temp=CCLayerColor::init();
	this->setTouchEnabled(true);
	return temp;
}

bool BasicLayer::initWithColor( const ccColor4B& color, GLfloat width, GLfloat height )
{
	bool temp=CCLayerColor::initWithColor(color,width,height);
	this->setTouchEnabled(true);
	return temp;
}

bool BasicLayer::initWithColor( const ccColor4B& color )
{
	bool temp=CCLayerColor::initWithColor(color);
	this->setTouchEnabled(true);
	return temp;
}

void BasicLayer::onEnter()
{
	CCLayerColor::onEnter();
}

void BasicLayer::onExit()
{
	CCLayerColor::onExit();
}

cocos2d::CCPoint BasicLayer::getMiddlePoint()
{
	return ccp(getWinSize().width/2,getWinSize().height/2);
}