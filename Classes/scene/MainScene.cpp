#include "MainScene.h"
#include "LoadingScene.h"
#include "basic\BasicDialog.h"


MainScene::MainScene(void)
{
}


MainScene::~MainScene(void)
{
}


bool MainScene::init()
{
	INIT_UP
		CC_BREAK_IF(!BasicLayer::initWithColor(ccc4(255,255,255,255)));
		CC_BREAK_IF(!initMenu());
		
		//加入标题
		CCLabelBMFont* title=CCLabelBMFont::create("LLKGame",BMFONT40);
		CC_BREAK_IF(!title);
		title->setPosition(getWinSize().width/2,getWinSize().height/4*3);
		this->addChild(title);

	INIT_DOWN
}

bool MainScene::initMenu()
{
	INIT_UP
		//开始按钮
		CCMenuItemLabel* startButton=CCMenuItemLabel::create(
		CCLabelBMFont::create("START",BMFONT40),
		this,
		menu_selector(MainScene::ButtonCallBack)
		);
	CC_BREAK_IF(!startButton);
	startButton->setPosition(getMiddlePoint());
	startButton->setUserObject(CCString::create("start"));

	CCMenu* mainMenu=CCMenu::create(startButton,NULL);
	CC_BREAK_IF(!mainMenu);
	mainMenu->setPosition(CCPointZero);
	this->addChild(mainMenu);

	INIT_DOWN
}

void MainScene::ButtonCallBack( CCObject* pSender )
{
	CCMenuItem* item=(CCMenuItem*)pSender;
	CCString* type=(CCString*)item->getUserObject();
	if(type->isEqual(CCString::create("start"))){
		//你按下了确定
		GameLog::showLog("Click Button");

		CCDirector::sharedDirector()->replaceScene(LoadingScene::scene());



	}else{
		//程序出差
		GameLog::showLog("Error");

	}

}
