#include "GameScene.h"



GameScene::GameScene(void)
{
	_myGameLayer=NULL;
	_myGameOverLayer=NULL;
	_myGameScoreLayer=NULL;
	_myControlLayer=NULL;


	_testLabel=NULL;


}


GameScene::~GameScene(void)
{
	_myGameLayer->release();
	_myGameOverLayer->release();
	_myGameScoreLayer->release();
	_myControlLayer->release();

	_testLabel->release();
}

bool GameScene::init()
{
	INIT_UP
		CC_BREAK_IF(!BasicLayer::init());
		//game����
		CCSprite* bg=CCSprite::create("game_bg.jpg");
		CC_BREAK_IF(!bg);
		bg->setPosition(getMiddlePoint());
		this->addChild(bg,BgOrder);

		//��Ϸ��
		GameLayer* gameLayer=GameLayer::create();
		CC_BREAK_IF(!gameLayer);
		setMyGameLayer(gameLayer);
		this->addChild(_myGameLayer,GameOrder);

		//������
		GameScoreLayer* gameScoreLayer=GameScoreLayer::create();
		CC_BREAK_IF(!gameScoreLayer);
		setMyGameScoreLayer(gameScoreLayer);
		this->addChild(_myGameScoreLayer,ScoreOrder);

		//������
		GameOverLayer* gameOverLayer=GameOverLayer::create();
		CC_BREAK_IF(!gameOverLayer);
		setMyGameOverLayer(gameOverLayer);
		this->addChild(_myGameOverLayer,OverOrder);

		//���Ʋ�
		ControlLayer* controlLayer=ControlLayer::create();
		CC_BREAK_IF(!controlLayer);
		setMyControlLayer(controlLayer);
		this->addChild(_myControlLayer,ControlOrder);


		//��������
		CCLabelBMFont* testLabel=CCLabelBMFont::create("Test",BMFONT40);
		CC_BREAK_IF(!testLabel);
		setTestLabel(testLabel);
		_testLabel->setPosition(getMiddlePoint());
		this->addChild(_testLabel,3);

	INIT_DOWN;
}
