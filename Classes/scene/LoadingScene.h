//  [9/13/2013 Limo]
/************************************************************************/
/* 进度读取的界面*/
/************************************************************************/
#pragma once
#include "util\GameHeader.h"
#include "basic\BasicLayer.h"
#include "basic\BasicScene.h"

class LoadingScene:public BasicLayer
{
public:
	LoadingScene(void);
	~LoadingScene(void);

	virtual bool init();
	CREATE_FUNC(LoadingScene);
	CREATE_SCENE(LoadingScene);

	CC_SYNTHESIZE_RETAIN(CCLabelBMFont*,_loadingLabel,LoadingLabel);	//loading的标签

	void LoadingTimer(float f);	//loading的定时器
	int index;

};

