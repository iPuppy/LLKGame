//  [9/13/2013 Limo]
/************************************************************************/
/* */
/************************************************************************/
#pragma once
#include "basic\BasicLayer.h"
#include "basic\BasicScene.h"
#include "util\GameHeader.h"
#include "layer\GameLayer.h"
#include "layer\GameOverLayer.h"
#include "layer\ControlLayer.h"
#include "layer\GameScoreLayer.h"
class GameScene:public BasicLayer
{
public:
	//z轴的位置
	enum{
		Min=0,
		BgOrder,				//背景层级
		GameOrder,				//游戏层级
		ScoreOrder,				//分数层级
		OverOrder,				//结束层级
		ControlOrder			//控制层级
	};

	GameScene(void);
	~GameScene(void);

	virtual bool init();
	CREATE_FUNC(GameScene);
	CREATE_SCENE(GameScene);

	CC_SYNTHESIZE_RETAIN(GameLayer*,_myGameLayer,MyGameLayer);					//游戏层
	CC_SYNTHESIZE_RETAIN(GameOverLayer*,_myGameOverLayer,MyGameOverLayer);		//游戏结束层
	CC_SYNTHESIZE_RETAIN(ControlLayer*,_myControlLayer,MyControlLayer);			//控制层
	CC_SYNTHESIZE_RETAIN(GameScoreLayer*,_myGameScoreLayer,MyGameScoreLayer);	//游戏分数层

	CC_SYNTHESIZE_RETAIN(CCLabelBMFont*,_testLabel,TestLabel);

};

