#include "LoadingScene.h"
#include "GameScene.h"


LoadingScene::LoadingScene(void)
{
	_loadingLabel=NULL;
	index=0;
}


LoadingScene::~LoadingScene(void)
{
	_loadingLabel->release();
}

bool LoadingScene::init()
{
	INIT_UP
		CC_BREAK_IF(!BasicLayer::init());
		//loding的背景
		CCSprite* bg=CCSprite::create("loading_bg.jpg");
		CC_BREAK_IF(!bg);
		bg->setPosition(getMiddlePoint());
		this->addChild(bg);

		//loading的文字
		CCLabelBMFont* label=CCLabelBMFont::create("0",BMFONT40);
		CC_BREAK_IF(!label);
		setLoadingLabel(label);
		_loadingLabel->setPosition(getMiddlePoint());
		this->addChild(_loadingLabel);

		//开启定时器
		this->schedule(schedule_selector(LoadingScene::LoadingTimer),.01f);

	INIT_DOWN
}

void LoadingScene::LoadingTimer( float f )
{
	char numStr[5]={0};
	sprintf(numStr,"%d",index);
	_loadingLabel->setString(numStr);

	index++;
	if(index>100){
		CCDirector::sharedDirector()->replaceScene(GameScene::scene());		//切换界面
		this->unschedule(schedule_selector(LoadingScene::LoadingTimer));	//关闭定时器
	}
}
