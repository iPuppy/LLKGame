//  [9/12/2013 Limo]
/************************************************************************/
/* 游戏的主界面*/
/************************************************************************/
#pragma once
#include "util\GameHeader.h"
#include "basic\BasicLayer.h"
#include "basic\BasicScene.h"

class MainScene:public BasicLayer
{
public:
	MainScene(void);
	~MainScene(void);

	virtual bool init();
	CREATE_FUNC(MainScene);
	CREATE_SCENE(MainScene);

	void ButtonCallBack(CCObject* pSender);		//按钮回调

private:
	bool initMenu();
};

