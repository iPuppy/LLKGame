//  [9/3/2013 Limo]
/************************************************************************/
/* cocos2dx的音乐操作工具类，用于管理音乐，实现自定义功能*/
/************************************************************************/
#pragma once
class Music
{
public:
	//单例模式
	static Music* GetInstance(){
		if(pMusic==0){
			pMusic=new Music();
		}
		return pMusic;
	}
	static void DelInstance()
	{
		if (pMusic)
		{
			delete pMusic;
			pMusic = 0;
		}
	}

public:
	void setMusicEnable(bool flag);			//设置是否需要声音
	void playMusic(const char* path, bool loop = false);//播放背景音乐，是否循环播放
	void loadMusic(const char* path);  //提前载入音频
	void stopMusic(bool bReleaseData = false); //停止音乐
	void pauseMusic();							//暂停背景音乐
	void resumeMusic();						//恢复音乐
	void rewindMusic();						//重新播放背景音乐
	bool isMusicPlaying();					//是否正在播放背景音乐
	float getMusicVolume();					//获得背景音乐音量
	void setMusicVolume(float volume);		//设置背景音乐音量



private:
	Music();					//构造方法
	static Music* pMusic;		//类的对象
	bool needMusic;				//是否需要音效
};

