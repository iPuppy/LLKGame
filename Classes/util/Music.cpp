#include "Music.h"
#include "SimpleAudioEngine.h"
#include "cocos2d.h"
USING_NS_CC;
using namespace CocosDenshion;

Music* Music::pMusic=0;

Music::Music(void)
{
}

void Music::setMusicEnable( bool flag )
{
		needMusic=flag;
}

void Music::playMusic( const char* path, bool loop /*= false*/ )
{
	if (needMusic)
	{
		if(!SimpleAudioEngine::sharedEngine()->isBackgroundMusicPlaying()){
			SimpleAudioEngine::sharedEngine()->playBackgroundMusic(CCFileUtils::sharedFileUtils()->fullPathForFilename(path).c_str(), loop);
		}else{
			SimpleAudioEngine::sharedEngine()->rewindBackgroundMusic();
		}
	}

}

void Music::loadMusic( const char* path )
{
	if(needMusic)
		SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic(CCFileUtils::sharedFileUtils()->fullPathForFilename(path).c_str());
}

void Music::stopMusic( bool bReleaseData /*= false*/ )
{
		SimpleAudioEngine::sharedEngine()->stopBackgroundMusic(bReleaseData);
}

void Music::pauseMusic()
{
		SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
}

void Music::resumeMusic()
{
	if(needMusic)
		SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
}

void Music::rewindMusic()
{
	if(needMusic)
		SimpleAudioEngine::sharedEngine()->rewindBackgroundMusic();
}

bool Music::isMusicPlaying()
{
	return SimpleAudioEngine::sharedEngine()->isBackgroundMusicPlaying();
}

float Music::getMusicVolume()
{
	return SimpleAudioEngine::sharedEngine()->getBackgroundMusicVolume();
}

void Music::setMusicVolume( float volume )
{
	if(needMusic)
		SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(volume);
}






