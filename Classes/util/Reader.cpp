#include "Reader.h"

Reader* Reader::pReader=0;

Reader::Reader(void)
{
	//初始化一个默认的字典
	pDict=CCDictionary::createWithContentsOfFile("reader.plist");
	pDict->retain();
}

const char* Reader::getCharByKey( const char* key )
{
	CCString* temCStr=((CCString*)pDict->valueForKey(key));
	if(temCStr->length()<=0){
		return NULL;
	}
	else{
		return temCStr->getCString();
	}

}

const int Reader::getIntByKey( const char* key )
{
	const char* temp=getCharByKey(key);
	if(temp){
		return atoi(temp);

	}else{
		return 0;

	}
}

const bool Reader::getBoolByKey( const char* key )
{
	const char* temp=getCharByKey(key);
	if(temp){
		if (strcmp(temp,"true")==0)
		{

			return true;
		}else{

			return false;

		}

	}else{
		return false;

	}
}

CCArray* Reader::getArrayByKey( const char* key )
{
	CCArray* array = (CCArray*)pDict->objectForKey(key);
	if(array->count()<=0){
		return NULL;
	}
	else{
	return array;
	}

}

void Reader::setDictPath( const char* fileName )
{
	//释放掉占用的文件
	pDict->release();
	pDict=CCDictionary::createWithContentsOfFile(fileName);
	pDict->retain();
}

