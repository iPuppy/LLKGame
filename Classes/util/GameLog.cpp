#include "GameLog.h"
USING_NS_CC;

void GameLog::showLog( const char* msg )
{
	if(DEUBG_SHOW_LOG)
		CCLOG("GameLog:%s",msg);
}

void GameLog::showLog( int i )
{
	if(DEUBG_SHOW_LOG)
		CCLOG("GameLog:%d",i);
}