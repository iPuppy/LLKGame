//  [9/18/2013 Limo]
/************************************************************************/
/* cocosdx从文件读取字符串的工具类 默认的文件是 资源根目录下的 reader.plist*/
/************************************************************************/
#pragma once
#include "cocos2d.h"
USING_NS_CC;
class Reader
{
public:

	//单例模式
	static Reader* GetInstance(){
		if(pReader==0){
			pReader=new Reader();
		}
		return pReader;
	}
	static void DelInstance()
	{
		if (pReader)
		{
			delete pReader;
			pReader = 0;
		}
	}
	void setDictPath(const char* fileName);				//设置当前要读取的字典文件
	const char* getCharByKey(const char* key);			//通过key获得字符串
	const int	getIntByKey(const char* key);			//通过key获得数字
	const bool	getBoolByKey(const char* key);			//通过key获得布尔变量
	CCArray*    getArrayByKey(const char* key);			//通过key获得数组

private:
	Reader();					//构造方法
	static Reader* pReader;		//类的对象
	CCDictionary *pDict;		//当前的字典集


};

