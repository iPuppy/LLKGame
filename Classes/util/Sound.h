//  [9/3/2013 Limo]
/************************************************************************/
/* cocos2dx的音效工具类，用于管理音效，实现自定义功能							*/
/************************************************************************/
#pragma once
class Sound
{
public:
	//单例模式
	static Sound* GetInstance(){
		if(pSound==0){
			pSound=new Sound();
		}
		return pSound;
	}
	static void DelInstance()
	{
		if (pSound)
		{
			delete pSound;
			pSound = 0;
		}
	}

public:
	void setSoundEnable(bool flag);					//设置是否需要音乐
	int playSound(char* path,bool loop=false);			//通过地址播放音效,path是到resource下的，所以直接放resource下的写文件名字就好,返回值为音效的id
	float getSoundsVolume();						//获得音效音量,对于某些设备不一定生效
	void setSoundsVolume(float volume);				//设置音效音量,对于某些设备不一定生效
	void pauseSoundById(int soundId);				//通过id暂停音效
	void pauseAllSound();							//暂停所有音效
	void resumeSoundById(int soundId);				//通过id恢复音效
	void resumeAllSound();							//恢复所有音效
	void stopSoundById(int soundId);				//通过id停止音效
	void stopAllSound();							//停止所有音效
	void loadSound(char* path);						//预先加载音效
	void unloadSound(char* path);					//卸载音效

private:
	Sound();					//构造方法
	static Sound* pSound;		//类的对象
	bool needSound;				//是否需要音效

};

