#include "Coder.h"
#include <string.h>
#include "cocos2d.h"
USING_NS_CC;
Coder* Coder::pCoder=0;

Coder::Coder(void)
{
	int localKey[]={1,2,3,4};
	key=localKey;
}
/************************************************************************/
/* 设置密匙种子,参数为int数组*/
/************************************************************************/
void Coder::setKey( int* val )
{
	key=val;
}
/************************************************************************/
/*	编码字符串 TODO:在编码的时候加入特定的字符串,实现编码后字符串的标记*/
/************************************************************************/
char* Coder::encodingNewChar( char* code )
{
	int len= sizeof(key);
	int n=strlen(code);
	CCLOG("n:%d",n);
	char* opt=new char[n+1];
	(*(opt+n))='\0';
	for(int i=0;i<n;i++){
		(*(opt+i))=(*(code+i))^*(key+i%(len-1));
	}
	return opt;
}
/************************************************************************/
/*	解码字符串 TODO:在解码的时候读取特定的字符串,判断是否是编码过的字符串,否则返回空*/
/************************************************************************/
char* Coder::decodingNewChar( char* code )
{
	int len= sizeof(key);
	int n=strlen(code);
	CCLOG("n:%d",n);
	char* opt=new char[n+1];
	(*(opt+n))='\0';
	for(int i=0;i<n;i++){
		(*(opt+i))=(*(code+i))^*(key+i%(len-1));
	}
	return opt;
}

