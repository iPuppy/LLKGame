//  [9/17/2013 Limo]
/************************************************************************/
/* 字符串加密的工具类														*/
/************************************************************************/
#pragma once
class Coder
{
public:
	//单例模式
	static Coder* GetInstance(){
		if(pCoder==0){
			pCoder=new Coder();
		}
		return pCoder;
	}
	static void DelInstance()
	{
		if (pCoder)
		{
			delete pCoder;
			pCoder = 0;
		}
	}

public:
	char* encodingNewChar(char* code);			//编码,返回值为new出的对象,需要delete
	char* decodingNewChar(char* code);			//解码,返回值为new出的对象,需要delete
	void setKey(int* val);				//设置编码种子

private:
	Coder();					//构造方法
	static Coder* pCoder;		//类的对象
	int* key;
};

