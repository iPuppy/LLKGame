#include "Sound.h"
#include "SimpleAudioEngine.h"
#include "cocos2d.h"
USING_NS_CC;
using namespace CocosDenshion;
Sound* Sound::pSound=0; //类的静态指针初始化

Sound::Sound(void)
{
}

void Sound::setSoundEnable( bool flag )
{
	needSound=flag;
}

int Sound::playSound( char* path,bool loop/*=false*/ )
{
	if(needSound){
		return SimpleAudioEngine::sharedEngine()->playEffect(CCFileUtils::sharedFileUtils()->fullPathForFilename(path).c_str(), loop);
	}
	return -1;
}

float Sound::getSoundsVolume()
{
	if(needSound){
		return SimpleAudioEngine::sharedEngine()->getEffectsVolume();
	}
	return 0;
}

void Sound::setSoundsVolume( float volume )
{
	if(needSound)
		SimpleAudioEngine::sharedEngine()->setEffectsVolume(volume);
}

void Sound::pauseSoundById( int soundId )
{
		SimpleAudioEngine::sharedEngine()->pauseEffect(soundId);
}

void Sound::pauseAllSound()
{
		SimpleAudioEngine::sharedEngine()->pauseAllEffects();
}

void Sound::resumeSoundById( int soundId )
{
	if(needSound)
		SimpleAudioEngine::sharedEngine()->resumeEffect(soundId);
}

void Sound::resumeAllSound()
{
	if(needSound)
		SimpleAudioEngine::sharedEngine()->resumeAllEffects();
}

void Sound::stopSoundById( int soundId )
{
		SimpleAudioEngine::sharedEngine()->stopEffect(soundId);
}

void Sound::stopAllSound()
{
		SimpleAudioEngine::sharedEngine()->stopAllEffects();
}

void Sound::loadSound( char* path )
{
	if(needSound)
		SimpleAudioEngine::sharedEngine()->preloadEffect(CCFileUtils::sharedFileUtils()->fullPathForFilename(path).c_str());
}

void Sound::unloadSound( char* path )
{
		SimpleAudioEngine::sharedEngine()->unloadEffect(CCFileUtils::sharedFileUtils()->fullPathForFilename(path).c_str());
}
